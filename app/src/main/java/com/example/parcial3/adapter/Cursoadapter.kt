package com.example.parcial3.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.parcial3.R
import com.example.parcial3.Superactividad2

class Cursoadapter(private val cursoList:List<Superactividad2>): RecyclerView.Adapter<Cursoholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Cursoholder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return Cursoholder(layoutInflater.inflate(R.layout.item_estudiante, parent ,false))

    }

    override fun onBindViewHolder(holder: Cursoholder, position: Int) {

        val item= cursoList[position]
        holder.render(item)

    }

    override fun getItemCount(): Int {
        return cursoList.size
    }

}