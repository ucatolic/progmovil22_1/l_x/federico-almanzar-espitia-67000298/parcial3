package com.example.parcial3.adapter

import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.parcial3.R
import com.example.parcial3.Superactividad2



class Cursoholder( val view: View): RecyclerView.ViewHolder(view) {



    val nombcur = view.findViewById<TextView>(R.id.tvcurso)
    val seme = view.findViewById<TextView>(R.id.semestre)
    val nompro = view.findViewById<TextView>(R.id.nomprofesor)
    val califcurso = view.findViewById<RatingBar>(R.id.califcurso)
    val califcurso2= view.findViewById<EditText>(R.id.calidcursox)
    val obligaci=view.findViewById<RadioGroup>(R.id.grupobotn)
    val photo = view.findViewById<ImageView>(R.id.animalx)

    fun render(superA :Superactividad2)
    {

        nombcur.text =superA.curso1
        seme.text =superA.semestre1
        nompro.text= superA.profesor1
       //califcurso.rating= superA.calificacion.toFloat()




        superA.photo?.let { photo.setImageResource(it) }

    }
}