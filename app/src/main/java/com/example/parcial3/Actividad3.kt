package com.example.parcial3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class Actividad3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividad3)
        setSupportActionBar(findViewById(R.id.toolbar))

        val aplicacion = application as Global

        val tvnombre: TextView = findViewById(R.id.tvpo)
        val tvcodigo: TextView = findViewById(R.id.tvpa)
        val tvemail: TextView = findViewById(R.id.tvv)
        val tvpromedio: TextView = findViewById(R.id.tvs)
        val tvelectivascur: TextView = findViewById(R.id.tvac)
        val btnBack: Button = findViewById(R.id.btnOr)

        tvnombre.text=aplicacion.nombreEs
        tvcodigo.text=""+aplicacion.codigoEs
        tvemail.text=aplicacion.emailEs


        btnBack.setOnClickListener {
            startActivity(Intent(this, Actividad2:: class.java))
            Toast.makeText(this, "Cursos", Toast.LENGTH_SHORT).show()
        }
    }
    //-------------------Toolbar---------------------//
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem)= when (item.itemId) {
        R.id.menu->{
            startActivity(Intent(this,Desarrollador::class.java))
            true
        }
        else->super.onOptionsItemSelected(item)
    }

}