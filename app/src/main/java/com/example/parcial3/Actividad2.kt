package com.example.parcial3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.RadioGroup
import android.widget.RatingBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.parcial3.adapter.Cursoadapter

class Actividad2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actividad2)
        setSupportActionBar(findViewById(R.id.toolbar))
        initRecycler()
        val boton2=findViewById<Button>(R.id.atras)
        boton2.setOnClickListener{
            val lanzar = Intent(this, MainActivity::class.java)
            startActivity(lanzar)
        }
        val boton3=findViewById<Button>(R.id.butonmirar)
        boton3.setOnClickListener{
            val lanzar = Intent(this, Actividad3::class.java)
            startActivity(lanzar)
        }
        //---------Llevando las variables al global----------//

    }
    private fun initRecycler()
    {
        val recycler: RecyclerView =findViewById(R.id.cursos2)
        recycler.layoutManager= LinearLayoutManager(this)
        recycler.adapter= Cursoadapter(Superactividadinfo.materiasList)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem)= when (item.itemId) {
        R.id.menu->{
            startActivity(Intent(this,Desarrollador::class.java))
            true
        }
        else->super.onOptionsItemSelected(item)
    }
}