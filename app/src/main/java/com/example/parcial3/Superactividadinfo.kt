package com.example.parcial3

class Superactividadinfo {
    companion object{
        val materiasList = listOf<Superactividad2>(
            Superactividad2(


                calificacion = 4,
                curso1 ="Fundamentacion" +
                        " matematica",
                semestre1 = "Sem. 1",
                profesor1 = "Cesar Ibañez",
                photo =R.drawable.calculador
            ),
            Superactividad2(

               calificacion = 4,
                curso1 = "Expresion oral y escrita",
                semestre1 = "Sem. 1",
                profesor1 = "Jairo Farfan Torres",
                photo = R.drawable.expresion
            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Preseminario",
                semestre1 = "Sem. 1",
                profesor1 = "jumm",
                photo = R.drawable.etica
            ),
            Superactividad2(

                calificacion = 3,
                curso1 = "Algoritmia y programacion",
                profesor1 = "jum",
                semestre1 = "Sem. 1",
                photo =R.drawable.codigo

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Pensamiento sistemico",
                profesor1 = "Gloria Concepción",
                semestre1 = "Sem. 1",
                photo =R.drawable.mecanismo

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Introduccion a la ingenieria",
                profesor1 = "jum",
                semestre1 = "Sem. 1",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Calculo Diferencial",
                profesor1 = "Cesar Ibañez",
                semestre1 = "Sem. 2",
                photo =R.drawable.calculo

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Algebra Lineal",
                profesor1 = "Katherine Serrano",
                semestre1 = "Sem. 2",
                photo =R.drawable.algebra

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Programación Imperativa",
                profesor1 = "No recuerdo",
                semestre1 = "Sem. 2",
                photo =R.drawable.programacion

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Estructuras Discretas",
                profesor1 = "No recuerdo",
                semestre1 = "Sem. 2",
                photo =R.drawable.estructura

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Electiva Institucional I",
                profesor1 = "No recuerdo",
                semestre1 = "Sem. 2",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Calculo Integral",
                profesor1 = "Cesar Ibañez",
                semestre1 = "Sem. 3",
                photo =R.drawable.red

            )
            ,
            Superactividad2(
                calificacion = 3,
                curso1 = "Mecanica yLab.",
                profesor1 = "Humberto",
                semestre1 = "Sem. 3",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Antropologia Filosofica",
                profesor1 = "No recuerdo",
                semestre1 = "Sem. 3",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Estructura de datos",
                profesor1 = "No recuerdo",
                semestre1 = "Sem. 3",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Programación orientada a Objetos",
                profesor1 = "Diego",
                semestre1 = "Sem. 3",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Calculo Vectorial",
                profesor1 = "Un viejito",
                semestre1 = "Sem. 4",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Probabilidad y estadistica",
                profesor1 = "Era facil",
                semestre1 = "Sem. 4",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Optica y Ondas",
                profesor1 = "No recuerdo",
                semestre1 = "Sem. 4",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Bases de datos",
                profesor1 = "Nixon",
                semestre1 = "Sem. 4",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Ingles Independiente",
                profesor1 = "Franki Quintero",
                semestre1 = "Sem. 4",
                photo =R.drawable.red

            ),
            Superactividad2(
                calificacion = 3,
                curso1 = "Ecuaciones Diferenciales",
                profesor1 = "Otro viejito",
                semestre1 = "Sem. 5",
                photo =R.drawable.red
            )
            ,
            Superactividad2(
                calificacion = 3,
                curso1 = "Electricidad y magnetismo",
                profesor1 = "Francisco Novegil",
                semestre1 = "Sem. 5",
                photo =R.drawable.red
            )
        )
    }
}
