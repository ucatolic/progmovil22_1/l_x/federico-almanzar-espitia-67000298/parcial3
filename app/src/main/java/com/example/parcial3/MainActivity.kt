package com.example.parcial3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.parcial3.adapter.Cursoadapter

var count: Int=0

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        //-------------- LLevando datos a la clase global------------//
        val aplicacion = application as Global
        val btn :Button = findViewById(R.id.Guardar)
        val nombre: EditText= findViewById(R.id.Nombre)
        val codigo: EditText=findViewById(R.id.Codigo)
        val email: EditText=findViewById(R.id.Email)

        btn.setOnClickListener(){
            if (nombre.text.toString() !="" && codigo.text.toString() !="" && email.text.toString() !="")
            {
                aplicacion.nombreEs = nombre.text.toString()
                aplicacion.codigoEs =(Integer(codigo.text.toString())).toInt()
                aplicacion.emailEs = email.text.toString()
                Toast.makeText(this,"Datos Guardados", Toast.LENGTH_SHORT).show()
                val lanzar = Intent(this, Actividad2::class.java)
                startActivity(lanzar)
                count=1
            }
            else if (nombre.text.toString() == "" || codigo.text.toString() =="" || email.text.toString() == "")
            {
                count=2
                Toast.makeText(this,"Ingrese los datos solicitados",Toast.LENGTH_SHORT).show()
            }
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem)= when (item.itemId) {
        R.id.menu->{
            startActivity(Intent(this,Desarrollador::class.java))
            true
        }
        else->super.onOptionsItemSelected(item)
    }





}