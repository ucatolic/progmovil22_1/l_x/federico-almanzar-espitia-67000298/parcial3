package com.example.parcial3

import androidx.annotation.DrawableRes

data class Superactividad2(

        val curso1: String,
        val semestre1: String,
        val profesor1: String,
        @DrawableRes
        val photo: Int?,
        val calificacion: Int,


)